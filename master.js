document.addEventListener('DOMContentLoaded', ()=>
  {
    const CMD = document.querySelector('#cmD'),
      NBD4 = document.querySelector('#nbD4'),
      NBD6 = document.querySelector('#nbD6'),
      NBD8 = document.querySelector('#nbD8'),
      NBD10 = document.querySelector('#nbD10'),
      NBD12 = document.querySelector('#nbD12'),
      NBD20 = document.querySelector('#nbD20'),
      NBDS = [NBD4, NBD6, NBD8, NBD10, NBD12, NBD20],
      D = ['4', '6', '8', '10', '12', '20']
      LANCER = document.querySelector('#lancer'),
      SOMME = document.querySelector('#somme .span'),
      MOYENNE = document.querySelector('#moy .span'),
      MIN = document.querySelector('#min .span'),
      MAX = document.querySelector('#max .span'),
      DICES = document.querySelector('#dices'),
      INPUTS = document.querySelectorAll('input[type="number"]'),
      DRAW = function(canvas, nbSides)
      {
        let cxt = canvas.getContext("2d"),
          cvHgt = canvas.height,
          size = cvHgt * 0.45,
          centerX = cvHgt * 0.5,
          centerY = cvHgt * 0.5,
          step  = 2 * Math.PI / nbSides,
          shift = (Math.PI / 180.0) * (nbSides == 5 ? -18 : nbSides == 3 ? 30 : nbSides == 4 ? 45 : 1),
          faceNb = canvas.id.slice(4);
  
        cxt.beginPath();
  
        if(nbSides != 0)
        {
          for(i = 0; i <= nbSides + 1; i++)
          {
            let curStep = i * step + shift;
            cxt.lineTo(centerX + size * Math.cos(curStep), centerY + size * Math.sin(curStep));
          }
        }
        else
        {
          cxt.lineTo(centerX, centerY - size);
          cxt.lineTo(centerX + 0.45 * size, centerY + 0.2 * size);
          cxt.lineTo(centerX, centerY + 0.5 * size);
          cxt.lineTo(centerX - 0.45 * size, centerY + 0.2 * size);
          cxt.lineTo(centerX, centerY - size);
          cxt.lineTo(centerX + 0.45 * size, centerY + 0.2 * size);
        }
  
        cxt.strokeStyle = "#111";
        cxt.fillStyle = "#ba3500";
        cxt.lineWidth = 10;
        cxt.stroke();
        cxt.fill();
  
        return 1;
      },
      CREATEDICE = function()
      {
        let futureD4 = Math.min(NBD4.value, 20),
          presentD4 = DICES.querySelectorAll('.dice.tetra').length,
          futureD6 = Math.min(NBD6.value, 20),
          presentD6 = DICES.querySelectorAll('.dice.hexa').length,
          futureD8 = Math.min(NBD8.value, 20),
          presentD8 = DICES.querySelectorAll('.dice.octo').length,
          futureD10 = Math.min(NBD10.value, 20),
          presentD10 = DICES.querySelectorAll('.dice.deca').length,
          futureD12 = Math.min(NBD12.value, 20),
          presentD12 = DICES.querySelectorAll('.dice.dodeca').length,
          futureD20 = Math.min(NBD20.value, 20),
          presentD20 = DICES.querySelectorAll('.dice.icosa').length,
          futureDices = [futureD4, futureD6, futureD8, futureD10, futureD12, futureD20],
          presentDices = [presentD4, presentD6, presentD8, presentD10, presentD12, presentD20];
  
        for(d = 0; d < futureDices.length; d++)
        {
          let nbFaces = d == 0 ? 4 : d == 1 ? 6 : d == 2 ? 8 : d == 3 ? 10 : d == 4 ? 12 : 20,
            nbSides = d == 1 ? 4 : d == 3 ? 0 : d == 4 ? 5 : 3,
            dName = d == 0 ? "tetra" : d == 1 ? "hexa" : d == 2 ? "octo" : d == 3 ? "deca" : d == 4 ? "dodeca" : "icosa";
  
          if(futureDices[d] > presentDices[d])
          {
            for(let i = presentDices[d]; i < futureDices[d]; i++)
            {
              let dice = document.createElement('div'),
                diceholder = document.createElement('div');
  
              dice.setAttribute('class', 'dice face1 ' + dName);
              diceholder.setAttribute('class', 'face1 diceholder ' + dName);
  
              for(j = 1; j <= nbFaces; j++)
              {
                let face = document.createElement('div'),
                  canvas = document.createElement('canvas'),
                  span = document.createElement('span');
  
                face.setAttribute('class', 'face face' + j);
                canvas.setAttribute('height', '500');
                canvas.setAttribute('width', '500');
                span.innerText = (dName == "deca" && j == 10 ? 0 : j);
  
                DRAW(canvas, nbSides);
  
                face.appendChild(canvas);
                face.appendChild(span);
                dice.appendChild(face);
              }
  
              diceholder.appendChild(dice);
              DICES.appendChild(diceholder);
            }
          }
          else
          {
            let dicesToRmv = document.querySelectorAll('#dices .diceholder.' + dName);
            for(let i = futureDices[d]; i < presentDices[d]; i++)
            {
              DICES.removeChild(dicesToRmv[i]);
            }
          }
        }
      };
  
    var dicesValue = new Array(),
      toThrow,
      canUpdate = true,
      writting = false,
      showStats = function()
      {
        if(dicesValue.length > 0)
        {
          sum = dicesValue.reduce((previousValue, currentValue) => previousValue + currentValue);
          SOMME.innerText = sum;
          MOYENNE.innerText = Math.round((sum / dicesValue.length) * 100) / 100;
          MIN.innerText = Math.min(...dicesValue);
          MAX.innerText = Math.max(...dicesValue);
        }
        else
        {
          SOMME.innerText = '0';
          MOYENNE.innerText = '0';
          MIN.innerText = '0';
          MAX.innerText = '0';
        }
  
        dicesValue = new Array();
      },
      throwDice = function()
      {
        changeTxt();
        CREATEDICE();
        let nbTours = 2 + Math.round(Math.random() * 5);
        for (var i = 0; i < nbTours; i++)
        {
          toThrow = setTimeout(function()
          {
            let presentDices = DICES.querySelectorAll('.dice')
  
            for(j = 0; j < presentDices.length; j++)
            {
              let classlist = presentDices[j].classList,
                nbFaces;
  
              if(classlist.contains('tetra'))
              {
                nbFaces = 4;
              }
              else if(classlist.contains('hexa'))
              {
                nbFaces = 6;
              }
              else  if(classlist.contains('octo'))
              {
                nbFaces = 8;
              }
              else  if(classlist.contains('deca'))
              {
                nbFaces = 10;
              }
              else  if(classlist.contains('dodeca'))
              {
                nbFaces = 12;
              }
              else  if(classlist.contains('icosa'))
              {
                nbFaces = 20;
              }
  
              for(k = 1; k <= nbFaces; k++)
              {
                if(presentDices[j].classList.contains('face' + k))
                {
                  presentDices[j].classList.remove('face' + k);
                }
              }
  
              let rng = Math.ceil(Math.random() * nbFaces);
              dicesValue.push(rng);
              presentDices[j].classList.add('face' + rng);
            }
  
            showStats();
          }, i * 450);
        }
      },
      changeTable = function()
      {
        let cmDvals = CMD.value.trim(),
          regex = /^([0-9]+d[0-9]+ ?\+? ?)+$/gi;
  
        for(i = 0; i < NBDS.length; i++)
        {
          NBDS[i].value = 0;
        }
  
        if(regex.test(cmDvals))
        {
          canUpdate = false;
          cmDvals = cmDvals.split("+");
  
          CMD.classList.remove('invalid');
  
          for(var i = 0; i < cmDvals.length; i++)
          {
            let val = cmDvals[i].trim();
  
            if(val != "")
            {
              let nbD = val.split("d")[0].trim(),
                dType = val.split('d')[1].trim();
  
              switch (dType)
              {
                case '4':
                  dType = 0;
                  break;
  
                case '6':
                  dType = 1;
                  break;
  
  
                case '8':
                  dType = 2;
                  break;
  
  
                case '10':
                  dType = 3;
                  break;
  
  
                case '12':
                  dType = 4;
                  break;
  
  
                case '20':
                  dType = 5;
                  break;
  
                default:
                  dType = -1;
              }
  
              if(dType != -1)
              {
                NBDS[dType].value = Math.min(parseInt(NBDS[dType].value) + parseInt(nbD), 20);
              }
            }
          }
  
          canUpdate = true;
  
          changeTxt();
          CREATEDICE();
        }
        else
        {
          CMD.classList.add('invalid');
        }
  
      },
      changeTxt = ()=>
      {
        if (canUpdate && !writting)
        {
          let txt = ""
          NBDS.forEach((nbd, i) =>
          {
            if(nbd.value > 0)
            {
              if(txt.length > 0)
              {
                txt = txt + " + "
              }
              txt = txt + nbd.value + "d" + D[i]
            }
          });
          CMD.value = txt
        }
      };
  
  
    document.addEventListener('keydown', (e)=>
    {
      if(e.code == 'Enter' || e.code == 'NumpadEnter')
      {
        throwDice();
      }
    });
  
    CMD.addEventListener('change', ()=>
    {
      writting = true;
      changeTable();
      writting = false;
    });
    CMD.addEventListener('input', ()=>
    {
      writting = true;
      changeTable();
      writting = false;
    });
    LANCER.addEventListener('click', ()=>
    {
      throwDice();
    });
    INPUTS.forEach((input) =>
    {
      input.addEventListener('change', changeTxt)
    });
  
    for(i = 0; i < NBDS.length; i++)
    {
      NBDS[i].addEventListener('change', function()
      {
        CREATEDICE();
      });
      NBDS[i].addEventListener('input', function()
      {
        CREATEDICE();
      });
    }
  });  